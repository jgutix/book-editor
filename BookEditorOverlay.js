'use strict';

var Component = require('substance/ui/Component');

function BookEditorOverlay() {
  Component.apply(this, arguments);
}

BookEditorOverlay.Prototype = function() {

  this.render = function($$) {
    var el = $$('div').addClass('sc-book-editor-overlay');
    var commandStates = this.props.commandStates;
    var toolRegistry = this.context.toolRegistry;

    toolRegistry.forEach(function(tool) {
      if (tool.options.overlay) {
        var toolProps = tool.Class.static.getProps(commandStates);
        if (toolProps) {
          el.append(
            $$(tool.Class, toolProps)
          );
        }
      }
    });
    return el;
  };
};

Component.extend(BookEditorOverlay);

module.exports = BookEditorOverlay;
