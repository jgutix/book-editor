'use strict';

var Component = require('substance/ui/Component');
var ToolGroup = require('substance/ui/ToolGroup');

function BookEditorToolbar() {
  Component.apply(this, arguments);
}

BookEditorToolbar.Prototype = function() {

  this.render = function($$) {
    var el = $$("div").addClass('sc-book-editor-toolbar');
    var commandStates = this.props.commandStates;
    var toolRegistry = this.context.toolRegistry;

    var tools = [];
    toolRegistry.forEach(function(tool, name) {
      if (!tool.options.overlay) {
        tools.push(
          $$(tool.Class, commandStates[name])
        );
      }
    });

    el.append(
      $$(ToolGroup).append(tools)
    );
    return el;
  };
};

Component.extend(BookEditorToolbar);
module.exports = BookEditorToolbar;
