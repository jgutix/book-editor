'use strict';

var Document = require('substance/model/Document');

function BookArticle(schema) {
  Document.call(this, schema);
  this._initialize();
}

BookArticle.Prototype = function() {

  this._initialize = function() {
    this.create({
      type: 'container',
      id: 'body',
      nodes: []
    });
  };

};

Document.extend(BookArticle);

module.exports = BookArticle;
