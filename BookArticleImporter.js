'use strict';

var HTMLImporter = require('substance/model/HTMLImporter');
var BookArticle = require('./BookArticle');
var schema = BookArticle.schema;

var converters = [
  require('substance/packages/paragraph/ParagraphHTMLConverter'),
  require('substance/packages/heading/HeadingHTMLConverter'),
  require('substance/packages/codeblock/CodeBlockHTMLConverter'),
  require('substance/packages/image/ImageHTMLConverter'),
  require('substance/packages/strong/StrongHTMLConverter'),
  require('substance/packages/emphasis/EmphasisHTMLConverter'),
  require('substance/packages/link/LinkHTMLConverter'),
];

function BookArticleImporter() {
  BookArticleImporter.super.call(this, {
    schema: schema,
    converters: converters,
    DocumentClass: BookArticle
  });
}

BookArticleImporter.Prototype = function() {
  /*
    Takes an HTML string.
  */
  this.convertDocument = function(bodyEls) {
    // Just to make sure we always get an array of elements
    if (!bodyEls.length) bodyEls = [bodyEls];
    this.convertContainer(bodyEls, 'body');
  };
};

HTMLImporter.extend(BookArticleImporter);

BookArticleImporter.converters = converters;

module.exports = BookArticleImporter;
